<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <form action="{{ route('create_post') }}" method="post" enctype="multipart/form-data" role="form">
            @csrf
            <div class="form-group">
                <label for="">Tên sản phẩm</label>
                <input type="text" name="name" id="" class="form-control" placeholder="" aria-describedby="helpId">
                @error('name')
                    <small style="color:red">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="">Giá</label>
                <input type="text" name="price" id="" class="form-control" placeholder="" aria-describedby="helpId">
                @error('price')
                    <small style="color:red">{{ $message }}</small>
                @enderror
            </div>
            <img src="" class="newimg" width="200px" alt="">
            <div class="form-group">
                <label for="file">Ảnh</label>
                <input type="file" name="img" id="file" class="form-control" placeholder="" aria-describedby="helpId">
                @error('img')
                    <small style="color:red">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="">Chi tiết</label>
                <input type="text" name="describe" id="" class="form-control" placeholder="" aria-describedby="helpId">
                @error('describe')
                    <small style="color:red">{{ $message }}</small>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Thêm</button>
        </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $('#file').change(function(e) {
            uploadfile(e.target.files[0])
        });

        function uploadfile(files) {
            var formData = new FormData();
            formData.append('img', files);
            $.ajax({
                type: 'POST',
                url: "{{ Route('uploadfile') }}",
                data: formData,
                processData: false,
                contentType: false,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                success: function(response) {
                    var src = "{{ FILE_ROOT }}" + response;
                    $(".newimg").attr("src", src);
                },
                error: function(error) {
                    alert('Error upload image');
                }
            })
        }
    </script>
</body>

</html>
