<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Carbon\Carbon;
use App\Models\Flights;
use App\Models\Admins;
use App\Services\FlightService;
use App\Services\UserTestService;
use App\Services\AdminService;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Notifications\ResetPasswordRequest;
use Illuminate\Support\Str;

class FlightsController extends Controller
{
    protected $flightservice;
    protected $usertestservice;
    protected $adminservice;
    public function __construct(FlightService $flightservice, UserTestService $usertestservice, AdminService $adminservice)
    {
        $this->flightservice = $flightservice;
        $this->usertestservice = $usertestservice;
        $this->adminservice = $adminservice;
    }
    public function test()
    {
        return view('welcome');
    }

    public function home()
    {
        $pro = Flights::all();
        return view('home', compact('pro'));
    }
    public function create()
    {
        return view('create');
    }

    public function uploadfile(Request $request)
    {
        $name_file = $request->file('img')->getClientOriginalName();
        $request->file('img')->move('site/img/', $name_file);
        echo $name_file;
    }

    public function create_post(ProductRequest $request)
    {

        $name_file = $request->file('img')->getClientOriginalName();
        $a = [
            'name' => $request->name,
            'price' => $request->price,
            'img' => $name_file,
            'describe' => $request->describe,
        ];
        $this->flightservice->create($a);
        return redirect()->route('home');
    }

    public function update($id)
    {
        $pro = $this->flightservice->findById($id);
        return view('update', compact('pro'));
    }

    public function update_post(ProductRequest $request, $id)
    {
        $name_file = $request->file('img')->getClientOriginalName();
        $a = [
            'name' => $request->name,
            'price' => $request->price,
            'img' => $name_file,
            'describe' => $request->describe,
        ];
        $this->flightservice->updateProduct($a, $id);
        return redirect()->route('home');
    }

    public function delete($id)
    {
        $this->flightservice->delete($id);
        return redirect()->route('home');
    }

    public function login()
    {
        return view('login');
    }
    public function login_post(LoginRequest $request)
    {
        $a = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        if (Auth::guard('admin')->attempt($a, $request->remember)) {
            return redirect()->route('home');
        }
    }
    public function registration()
    {
        return view('registration');
    }

    public function registration_post(Request $request)
    {
        $a = [
            'name_admin'  => $request->name,
            'email'      => $request->email,
            'password'   => bcrypt($request->password),
            'position'   => $request->position,
        ];
        $this->adminservice->addadmin($a);
        return redirect()->route('login');
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('login');
    }

    public function send_mail()
    {
        $name = 'Test mail';
        Mail::send('SendMail', compact('name'), function ($email) {
            $email->to('quangma31121999@gmail.com', 'Bản Test');
        });
    }
    public function viewSendMailResetPass()
    {
        return view('ResetPassword');
    }

    public function sendMailResetPass(Request $request)
    {
        $user = Admins::where('email', $request->email)->firstOrFail();
        $passwordReset = Admins::updateOrCreate([
            'email' => $user->email,
        ], [
            'token' => Str::random(60),
        ]);

        if ($passwordReset) {
            $user->notify(new ResetPasswordRequest($passwordReset->token));
        }
        return response()->json([
            'message' => 'Hãy mở mail và đặt lại mật khẩu mới'
        ]);
    }
    public function changePassword($token)
    {
        return view('formChangePassword', compact('token'));
    }

    public function changePasswordPost(Request $request, $token)
    {
        $a = explode("=",$token);
        $passwordReset = Admins::where('token',  $a[1])->firstOrFail();
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.',
            ], 422);
        }
        $pass = bcrypt($request->password);
        $user = Admins::where('email', $passwordReset->email)->firstOrFail();
        $updatePassword = $user->update(['password'=> $pass]);
        if($updatePassword){
            $updatePassword = $user->update(['token'=> null]);
        }
        return redirect()->route('login');
    }
}
