<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FlightsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [FlightsController::class, 'test']);

Route::get('/login', [FlightsController::class, 'login'])->name('login');
Route::get('/registration', [FlightsController::class, 'registration'])->name('registration');
Route::post('/registration', [FlightsController::class, 'registration_post'])->name('registration_post');
Route::post('/login', [FlightsController::class, 'login_post'])->name('login_post');
Route::get('/send-mail', [FlightsController::class, 'send_mail'])->name('send_mail');
Route::get('/reset-password', [FlightsController::class, 'viewSendMailResetPass'])->name('reset_password');
Route::post('/reset-password', [FlightsController::class, 'sendMailResetPass'])->name('reset_password_post');
Route::get('/change-password/{token}', [FlightsController::class, 'changePassword'])->name('changePassword');
Route::post('/change-password/{token}', [FlightsController::class, 'changePasswordPost'])->name('changePasswordPost');

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::get('/home', [FlightsController::class, 'home'])->name('home');
    Route::get('/logout', [FlightsController::class, 'logout'])->name('logout');
    Route::get('/create', [FlightsController::class, 'create'])->name('create');
    Route::post('/create', [FlightsController::class, 'create_post'])->name('create_post');
    Route::get('/update/{id}', [FlightsController::class, 'update'])->name('update');
    Route::post('/update/{id}', [FlightsController::class, 'update_post'])->name('update_post');
    Route::post('/uploadfile', [FlightsController::class, 'uploadfile'])->name('uploadfile');
    Route::get('/delete/{id}', [FlightsController::class, 'delete'])->name('delete');
});
